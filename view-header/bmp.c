#include "bmp.h"
#include "util.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint-gcc.h>
#include <stdint.h>
#include <stdarg.h>
#define BM 0x4D42
#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");


void bmp_header_print( struct bmp_header const* header, FILE* f ) {
   FOR_BMP_HEADER( PRINT_FIELD )
}

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

bool read_header_from_file( const char* filename, struct bmp_header* header ) {
    if (!filename) return false;
    FILE* f = fopen( filename, "rb" );
    if (!f) return false;
    if (read_header( f, header ) ) {
        fclose( f );
        return true;
    }

    fclose( f );
    return false;
}

/*  Считаем padding */
static uint32_t getpadding (const uint32_t width)
{
    return (4-(width*3%4))%4;
}

/*  Создание заголовка */
static struct bmp_header create_header(uint64_t width, uint64_t height){
    return (struct bmp_header) {
        /* Почему: http://math.ivanovo.ac.ru/dalgebra/Khashin/gr/bmp/bmp.html */
            .bfType = BM,
            .bfileSize = 24*width*height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = sizeof(struct bmp_header) + width*height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

/*  Функция для открытия файла и запуска deserializer */
enum read_status from_opening_bmp(const char* filename, struct image *image){

    FILE *in = fopen(filename, "rb");
    if(!in) return READ_ERROR;
    enum read_status from = from_bmp(in, image);
    if(fclose(in) != 0) return READ_CLOSING_ERROR;
    return from;
}
/* deserializer */
enum read_status from_bmp(FILE* in, struct image* img)
{

     /* Считываем новый заголовок */
     struct bmp_header* header = {0};
     if(!read_header(in, header)) return READ_INVALID_HEADER;;

    // Ошибка READ_INVALID_SIGNATURE
    /* Почему: http://math.ivanovo.ac.ru/dalgebra/Khashin/gr/bmp/bmp.html */
	if (header->bfType!=BM) return READ_INVALID_SIGNATURE;


    // Ошибка READ_INVALID_BITS
    /* Логика: http://math.ivanovo.ac.ru/dalgebra/Khashin/gr/bmp/bmp.html */
	if (header->bOffBits!=header->biSize+14) return READ_INVALID_BITS;


    /* Ширину, длину записываем в img */
   const uint32_t  width = header->biWidth;
   const uint32_t height = header->biHeight;
    *img = (struct image){
    .width=width,
    .height=height,
    .data=malloc(height*width*sizeof (struct pixel))
    };

    /* Считаем padding через функцию */
    const size_t padding = getpadding(width);

    /* Пиксели в пиксели */
    for(size_t i=0; i<height; i++){
        if (!fread(&(img->data[i*width]), sizeof (struct pixel), width, in)) return READ_ERROR;;
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;

}

/*  Функция для открытия для записи файла и запуска serializer,
так же возвращает все возможные ошибки */
enum write_status to_create_bmp(const char* filename, struct image const* img )
{
    FILE *out = fopen(filename, "wb");
    if(!out) return WRITE_ERROR;
    if(to_bmp(out, img) != WRITE_OK) return to_bmp(out, img);
    if(fclose(out) != 0) return WRITE_CLOSING_ERROR;
    return WRITE_OK;
}
/*  serializer */
enum write_status to_bmp(FILE* out, struct image const* img )
{
    const uint32_t padding = getpadding(img->width);
    /*  Получим новый заголовок */
    struct bmp_header header = create_header(img->width, img->height);
    /* Записываем заголовок */
    fwrite(&header, sizeof(struct bmp_header), 1, out);
    /* Создаем пустой символ, чтобы ставить после */
    const uint8_t r=0;
    /* Запись */
    for (size_t i = 0; i < img->height; i++) {
        if(!fwrite(&img->data[i*img->width], sizeof(struct pixel), img->width, out)) return WRITING_ERROR;
        if(!fwrite(&r, 1, padding, out)) return WRITING_ERROR;
    }
    return WRITE_OK;
}
