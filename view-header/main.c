#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdint-gcc.h>
#include <stdint.h>
#include <stdbool.h>

#include "bmp.h"
#include "util.h"
#include "rotate.h"

/*  Массив строк с индексами статусов
для вывода ошибок   */
static const char *read_err[] = {
        [READ_INVALID_SIGNATURE] = "Invalid signature",
        [READ_INVALID_BITS] = "Invalid Bits",
        [READ_INVALID_HEADER] = "Invalid Header",
		[READ_ERROR] = "Read error",
        [READ_CLOSING_ERROR] = "Closing file error"
};

static const char *write_err[] = {
        [WRITE_ERROR] = "Opening to write error",
        [WRITE_CLOSING_ERROR] = "Closing file error",
        [WRITING_ERROR] = "Writing error"
};

struct image rotating (const char* argv, struct image const* img)
{
    if (strcmp(argv, "right")==0) return rotate_right(img);
    if (strcmp(argv, "left")==0) return rotate_left(img);
}

void img_free(struct image a) { 
        free(a.data); 
        a.width = 0;
        a.height = 0;
}

void usage() {
    fprintf(stderr, "For right rotate: ./rotate right BMP_FILE_NAME\n");
    fprintf(stderr, "Or for left: ./rotate left BMP_FILE_NAME\n");
}

int main( int argc, char** argv ) {
    if (argc != 3) usage();
    if (argc < 3) err("Not enough arguments \n" );
    if (argc > 3) err("Too many arguments \n" );

    struct image image_old = {0};
    enum read_status old_open = from_opening_bmp(argv[2], &image_old);
    if (old_open != READ_OK) {
        err("Error: %s\n", read_err[old_open]);
        return 0;
    }

    fprintf(stdout, "Processing...\n");
    struct image new_img=rotating (argv[1], &image_old);
    fprintf(stdout, "Rotate: Ok.\n");
    img_free(image_old);

    enum write_status new_open = to_create_bmp(argv[2], &new_img);
    if (new_open != WRITE_OK) {
        err("Error: %s\n", write_err[new_open]);
        return 0;
    }
    fprintf(stdout, "Writing: Ok.\n");
    img_free(new_img);
    return 0;
}
