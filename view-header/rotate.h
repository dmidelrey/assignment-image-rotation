#ifndef _ROTATE_H_
#define _ROTATE_H_

#include <stdio.h>
#include <stdint.h>

struct image rotate_right(struct image const * source);
struct image rotate_left(struct image const * source);

#endif