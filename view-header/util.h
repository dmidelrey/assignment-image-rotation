#ifndef _UTIL_H_
#define _UTIL_H_
#include <stdint.h>
#include <inttypes.h>


_Noreturn void err( const char* msg, ... );
#endif
