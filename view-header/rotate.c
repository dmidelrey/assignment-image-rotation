#include "rotate.h"
#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint-gcc.h>
#include <stdint.h>
#include <stdarg.h>

/* Переворот вправо */
struct image rotate_right(struct image const * source)
{
    const uint32_t width = source->width;
    const uint32_t height = source->height;
    /* Так как переворот на 90 градусов
    Прямой переворот, значит меняем местами
    ширину и длину */
    struct image new_img = {
            .width = height,
            .height = width,
            .data = malloc(sizeof (struct pixel) *height*width)
    };
    /* Переворот вправо */
    for (uint32_t i = 0; i<new_img.height; i++) {
		for (uint32_t j = 0; j<new_img.width; j++) {
			*(new_img.data + i*new_img.width + j) = *(source->data + (new_img.width - j - 1)*new_img.height + i);
		}
	}
    return new_img;
}

/* Переворот влево */
struct image rotate_left(struct image const * source)
{
    const uint32_t width = source->width;
    const uint32_t height = source->height;
    /* Так как переворот на 90 градусов
    Прямой переворот, значит меняем местами
    ширину и длину */
    struct image new_img = {
            .width = height,
            .height = width,
            .data = malloc(sizeof (struct pixel) *height*width)
    };
    /* Переворот влево */
    for (uint32_t i = 0; i<new_img.height; i++) {
		for (uint32_t j = 0; j<new_img.width; j++) {
			*(new_img.data + i*new_img.width + j) = *(source->data + j*source->width + (new_img.width-1-i));
		}
	}
    return new_img;
}
